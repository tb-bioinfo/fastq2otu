
import os
import sys
import argparse

from split_kraken import split_kraken
from kraken2OTUtable.kraken2otu import read_in_files, create_otu_table

'''
# run from project folder

cd /path/to/project/folder/
PATH_TO_FASTQ2OTU=$HOME/Documents/projects/fastq2otu/
cp $PATH_TO_FASTQ2OTU/databases_*.txt $PATH_TO_FASTQ2OTU/taxa_*tsv ./

# TODO add config file for kraken2 with threads and path to exe #-c project/XX.yaml

python3 $PATH_TO_FASTQ2OTU/fastq2otu.py -d databases_16S.txt -t taxa_16S.tsv -i 16S  -o ./ ;
python3 $PATH_TO_FASTQ2OTU/fastq2otu.py -d databases_18S-ITS.txt -t taxa_18S-euk.tsv -i 18S -o ./ ;
python3 $PATH_TO_FASTQ2OTU/fastq2otu.py -d databases_18S-ITS.txt -t taxa_ITS-euk.tsv -i ITS -o ./ 
'''

KRAKEN_REPORTS_FOLDER = "kraken_reports"

parser = argparse.ArgumentParser()
parser.add_argument("--input-folder", "-i", type=str, required=True,
                    help="Input folder containing subfolders where the Fastq files can be found.")
parser.add_argument("--db-file", "-d", type=str, required=True,
                    help="file containing databases list.")
parser.add_argument("--taxa-file", "-t", type=str, required=True,
                    help="taxa to extract from taxonomy tree.")                  
parser.add_argument("--output-folder", "-o", type=str, required=True,
                    help="Output folder where the OTU tables and intermediate Kraken 2 reports will be stored.")
#parser.add_argument("--config-file", "-c", type=str, required=True,
#                   help="Config yaml with kraken2 parameters.")
args = parser.parse_args()

threads = 10 #config["KRAKEN2_THREADS"]
kraken2 = "kraken2" # config["KRAKEN2_BIN"]
# Retrieve databases list

db_file = args.db_file
databases = []
with open(db_file, "r") as tfile:
    lines = tfile.readlines()
    for line in lines:
        databases += line.rstrip('\n').split(',')
print(databases)

# For each database
for database in databases:
    os.makedirs(os.path.join(args.output_folder, KRAKEN_REPORTS_FOLDER, os.path.basename(database)), exist_ok=True)
    # For each sample
    for folder in [directory for directory in os.listdir(args.input_folder) if os.path.isdir(os.path.join(args.input_folder, directory))]:
        if folder!=KRAKEN_REPORTS_FOLDER:
            # Run Kraken
            kraken_command = f"{kraken2} --threads {threads} --db \"{database}\" --report \"{os.path.join(args.output_folder, KRAKEN_REPORTS_FOLDER, os.path.basename(database), folder)}.{args.input_folder}.k2report\" {os.path.join(args.input_folder, folder, '*')} >/dev/null"
            print(kraken_command)
            os.system(kraken_command)
            
            print("Split report")
            taxa = split_kraken(os.path.join(args.output_folder, KRAKEN_REPORTS_FOLDER, os.path.basename(database), f"{folder}.{args.input_folder}.k2report"),
                args.taxa_file)
            
    for taxon in taxa:
        print(f"Create OTU table for {database} and {taxon}")
        file_dicts = read_in_files(os.path.join(args.output_folder, KRAKEN_REPORTS_FOLDER, os.path.basename(database), taxon))
        create_otu_table(None, file_dicts, outdir=args.output_folder, file_name_suffix=f"_{taxon}_{os.path.basename(database)}")

