#python3 /home/thomas/Documents/projects/fastq2otu/fastq2otu.py -i /media/thomas/5ECC8BE5CC8BB5B5/MinION_analysis/Basecalled/Adouane/16S -o /media/thomas/5ECC8BE5CC8BB5B5/MinION_analysis/Basecalled/Adouane


import os
import sys
import argparse

from split_kraken import split_kraken
from kraken2OTUtable.kraken2otu import read_in_files, create_otu_table

KRAKEN_REPORTS_FOLDER = "kraken_reports"

parser = argparse.ArgumentParser()
parser.add_argument("--input-folder", "-i", type=str, required=True,
                    help="Input folder containing subfolders where the Fastq files can be found.")
parser.add_argument("--output-folder", "-o", type=str, required=True,
                    help="Output folder where the OTU tables and intermediate Kraken 2 reports will be stored.")
args = parser.parse_args()

# Retrieve databases list
db_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'databases.csv')
databases = []
with open(db_file, "r") as tfile:
    lines = tfile.readlines()
    for line in lines:
        databases += line.rstrip('\n').split(',')
print(databases)

# For each database
for database in databases:
    os.makedirs(os.path.join(args.output_folder, KRAKEN_REPORTS_FOLDER, os.path.basename(database)), exist_ok=True)
    # For each sample
    for folder in [directory for directory in os.listdir(args.input_folder) if os.path.isdir(os.path.join(args.input_folder, directory))]:
        if folder!=KRAKEN_REPORTS_FOLDER:
            # Run Kraken
            kraken_command = f"kraken2 --db \"{database}\" --report \"{os.path.join(args.output_folder, KRAKEN_REPORTS_FOLDER, os.path.basename(database), folder)}.k2report\" {os.path.join(args.input_folder, folder, '*')} >/dev/null"
            print(kraken_command)
            os.system(kraken_command)
            
            print("Split report")
            taxa = split_kraken(os.path.join(args.output_folder, KRAKEN_REPORTS_FOLDER, os.path.basename(database), f"{folder}.k2report"))
            
    for taxon in taxa:
        print(f"Create OTU table for {database} and {taxon}")
        file_dicts = read_in_files(os.path.join(args.output_folder, KRAKEN_REPORTS_FOLDER, os.path.basename(database), taxon))
        create_otu_table(None, file_dicts, outdir=args.output_folder, file_name_suffix=f"_{taxon}_{os.path.basename(database)}")

