# Fastq2otu

Converts a set of *Fastq* files to OTUs tables using [*Kraken 2*](https://github.com/DerrickWood/kraken2).

## Table of Contents

- [Description](#description)
- [Dependencies](#dependencies)
- [Configuration](#configuration)
- [Usage](#usage)

## Description

*Fastq2otu* first runs *Kraken 2* to generate reports and generates OTUs tables from them.

## Dependencies

*Fastq2otu* uses [*Kraken 2*](https://github.com/DerrickWood/kraken2) to generate the OTUs tables, so you have to install it.

## Configuration

*Fastq2otu* relies on two configuration files to work: `databases.csv` and `taxa.csv`. One OTUs table will be generated for each database-taxon pair.

### databases.csv

This file contains the path to the *Kraken 2* databases to use, one database per line.

### taxa.csv

This file contains the list of taxa that you want to keep in the OTUs tables in the following format:

```
file_name	taxonomic_rank	taxon_name
```

where `file_name` is the name that will appear in the name of the file that will be created, `taxonomic_rank` is the taxonomic rank as displayed in the *Kraken 2* report and `taxon_name` is the name of the taxon as displayed in the *Kraken 2* report. To use the whole report, you can just enter `all	R	root` in the file.

## Usage

```
usage: fastq2otu.py --db-file <DB_FILE> --taxa-file <TAXA_FILE> --input-folder <INPUT_FOLDER> --output-folder <OUTPUT_FOLDER>

optional arguments:
  -h, --help                           Show this help message and exit
  -d, --db-file                        file containing databases list
  -t, --taxa-file                      taxa to extract from taxonomy tree
  -i, --input-folder INPUT_FOLDER      Input folder containing subfolders where the Fastq files can be found.
  -o, --output-folder OUTPUT_FOLDER    Output folder where the OTU tables and intermediate Kraken 2 reports will be stored.
```
