import os
import argparse

def usage():
    parser.print_help()

# Split a Kraken 2 report to keep only the desired taxa
def split_kraken(kraken_report, taxa_file, output_folder=None):
    kraken_report_folder, kraken_report_basename = os.path.split(kraken_report)
    if output_folder is None:
        output_folder = kraken_report_folder
        
    with open(taxa_file, "r") as tfile:
        print(tfile)
        lines = tfile.readlines()
        TAXA = {line.split('\t')[0]:[line.split('\t')[1], line.split('\t')[2].rstrip('\n')] for line in lines if len(line.split('\t'))>=3}
        print(TAXA)

    with open(kraken_report, "r") as kfile:
        lines = kfile.readlines()

    rank_index = 3
    taxon_index = 5

    for folder in TAXA.keys():
        os.makedirs(f"{output_folder}/{folder}", exist_ok=True)
        os.remove(f"{output_folder}/{folder}/{kraken_report_basename}") if os.path.exists(f"{output_folder}/{folder}/{kraken_report_basename}") else None

    active_taxa = []
    indents = {}

    file_handles = {folder:open(f"{output_folder}/{folder}/{kraken_report_basename}", "a") for folder in TAXA.keys()}

    # Parse Kraken report line by line
    for line in lines:
        line_info = line.rstrip("\n").split("\t")
        rank = line_info[rank_index].strip()
        taxon = line_info[taxon_index].strip()
        indent = len(line_info[taxon_index]) - len(taxon)
        for active in list(active_taxa):
            if indent<=indents[active]:
                active_taxa.remove(active)
        for key, value in TAXA.items():
            if value==[rank, taxon]:
                active_taxa.append(key)
                indents[key] = indent

        for active in active_taxa:
            file_handles[active].write(line)

    for file_handle in file_handles.values():
        file_handle.close()

    return TAXA.keys()

if __name__ == "__main__":
    parser=argparse.ArgumentParser(description="""Program to extract information of a given clade from Kraken 2 report""")
    parser.add_argument('-k', '--kraken-report', help='The Kraken 2 report to split.', type=str)
    args = parser.parse_args()
    split_kraken(args.kraken_report, ".")